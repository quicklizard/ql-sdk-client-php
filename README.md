# Quicklizard SDK Client - PHP

Send SDK product and payment confirmation events from your server side using PHP.

## Installation

Add this line to your application's composer.json:

```json
{
  "require": {
    "quicklizard/sdk-client": "master"
  },
  "repositories": [
    {
      "type": "vcs",
      "url":  "git@bitbucket.org:quicklizard/ql-sdk-client-php.git"
    }
  ]
}
```

And then execute:

    $ composer install

## Usage

```php
//replace CLIENT_KEY with your actual QL client key
$client = new QL\SDK\Client('CLIENT_KEY');
```

### Product event

```php
$productId = "123-11-BB";
$shelfPrice = 123.23; //NOTE - shelfPrice must be a double
$permalink = "https://mystore.com/products/123-11-BB";
$client->sendProductEvent($productId, $shelfPrice, $permalink);
```

#### Product event with optional attributes

```php
$productId = "123-11-BB";
$shelfPrice = 123.23; //NOTE - shelfPrice must be a double
$permalink = "https://mystore.com/products/123-11-BB";
$attrs = array("inventory" => 1, "brand" => "Apple");
$client->sendProductEvent($productId, $shelfPrice, $permalink, $attrs);
```
---
### Payment confirmation event

```php
$productId = "123-11-BB";
$salePrice = 123.23; //NOTE - salePrice must be a double
$permalink = "https://mystore.com/products/123-11-BB";
$client->sendConfirmationEvent($productId, $salePrice);
```
---

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/quicklizard/ql-sdk-client-php.

## License

The code is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

