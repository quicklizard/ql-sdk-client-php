<?php

namespace QL\SDK;

class ClientTest extends \PHPUnit_Framework_TestCase
{
  
  private $client;
  
  protected function setUp()
  {
    $this->client = new Client('test-test');
  }
  
  /**
   * Test buildEventDataObject on product event
   */
  public function testBuildEventDataObject_Product()
  {
    $productId = "123456";
    $shelfPrice = 123.24;
    $permalink = "https://mysite.com/product/123";
    $dataA = $this->client->buildEventDataObject($productId, $shelfPrice, $permalink);
    $this->assertEquals($productId, $dataA["uid"]);
    $this->assertEquals($shelfPrice, $dataA["meta"]["price"]);
    $this->assertEquals($permalink, $dataA["meta"]["permalink"]);
    $dataB = $this->client->buildEventDataObject($productId, $shelfPrice, "");
    $this->assertEquals($productId, $dataB["uid"]);
    $this->assertEquals($shelfPrice, $dataB["meta"]["price"]);
    $this->assertArrayNotHasKey("permalink", $dataB["meta"]);
  }
  
  /**
   * Test buildEventDataObject on confirmation event
   */
  public function testBuildEventDataObject_Confirmation()
  {
    $productId = "123456";
    $salePrice = 123.24;
    $data = $this->client->buildEventDataObject($productId, $salePrice);
    $this->assertEquals($productId, $data["uid"]);
    $this->assertEquals($salePrice, $data["meta"]["price"]);
    $this->assertArrayNotHasKey("permalink", $data["meta"]);
  }
  
  /**
   * Test product event validation
   */
  public function testValidateProductEvent()
  {
    $productId = "123456";
    $shelfPrice = 123.24;
    $permalink = "https://mysite.com/product/123";
    $validationA = $this->client->validateProductEvent($productId, $shelfPrice, $permalink);
    $this->assertTrue($validationA["valid"]);
    $this->assertNull($validationA["exception"]);
    $validationB = $this->client->validateProductEvent("", $shelfPrice, $permalink);
    $this->assertFalse($validationB["valid"]);
    $this->assertInstanceOf('InvalidArgumentException', $validationB["exception"]);
    $validationC = $this->client->validateProductEvent($productId, "23,4", $permalink);
    $this->assertFalse($validationC["valid"]);
    $this->assertInstanceOf('InvalidArgumentException', $validationC["exception"]);
    $validationD = $this->client->validateProductEvent($productId, $shelfPrice, "");
    $this->assertFalse($validationD["valid"]);
    $this->assertInstanceOf('InvalidArgumentException', $validationD["exception"]);
  }
  
  /**
   * Test confirmation event validation
   */
  public function testValidateConfirmationEvent()
  {
    $productId = "123456";
    $salePrice = 123.24;
    $validationA = $this->client->validateConfirmationEvent($productId, $salePrice);
    $this->assertTrue($validationA["valid"]);
    $this->assertNull($validationA["exception"]);
    $validationB = $this->client->validateConfirmationEvent("", $salePrice);
    $this->assertFalse($validationB["valid"]);
    $this->assertInstanceOf('InvalidArgumentException', $validationB["exception"]);
    $validationC = $this->client->validateConfirmationEvent($productId, "23,4");
    $this->assertFalse($validationC["valid"]);
    $this->assertInstanceOf('InvalidArgumentException', $validationC["exception"]);
  }
  
  /**
   * Test confirmation event validation
   */
  public function testBuildEventURL_Product()
  {
    $productId = "123456";
    $shelfPrice = 123.24;
    $permalink = "https://mysite.com/product/123";
    $attrs = array("inventory" => 1, "brand" => "Apple");
    $dataA = $this->client->buildEventDataObject($productId, $shelfPrice, $permalink, $attrs);
    $url = $this->client->buildEventURL("product", $dataA);
    $this->assertContains("https://evt.quicklizard.com/event.gif?event=", $url);
    $this->assertContains('%22name%22%3A%22product%22', $url);
    $this->assertContains('%22brand%22%3A%22Apple%22', $url);
  }
  
  /**
   * Test confirmation event validation
   */
  public function testBuildEventURL_Confirmation()
  {
    $productId = "123456";
    $shelfPrice = 123.24;
    $dataA = $this->client->buildEventDataObject($productId, $shelfPrice);
    $url = $this->client->buildEventURL("confirmation", $dataA);
    $this->assertContains("https://evt.quicklizard.com/event.gif?event=", $url);
    $this->assertContains('%22name%22%3A%22confirmation%22', $url);
  }
  
  /**
   * Test confirmation event validation
   */
  public function testSendEvent()
  {
    $productId = "123456";
    $shelfPrice = 123.24;
    $dataA = $this->client->buildEventDataObject($productId, $shelfPrice);
    $response = $this->client->sendEvent("confirmation", $dataA);
    $this->assertEquals(200, $response["status_code"]);
  }
}