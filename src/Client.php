<?php

namespace QL\SDK;

class Client
{
  
  const EVENTS = array("product" => "product", "conversion" => "confirmation");
  const EVT_URL = 'https://evt.quicklizard.com/event.gif';
  
  /** @var null|string */
  private $client_key = null;
  
  /**
   * Create a new Client Instance
   *
   * @param string $client_key
   */
  public function __construct($client_key)
  {
    $this->client_key = $client_key;
  }
  
  /**
   * Send product page view event to QL
   * @param $productId (string) - globally unique product id
   * @param $shelfPrice (double) - product shelf price (must be a positive double)
   * @param $permalink (string) - product page permalink
   * @param $attrs (array) - associative array of additional product attributes
   * @return array SDK request response object with body, errors and HTTP status
   */
  public function sendProductEvent($productId, $shelfPrice, $permalink, $attrs = array())
  {
    $validationResult = $this->validateProductEvent($productId, $shelfPrice, $permalink);
    if ($validationResult["valid"] === false) {
      throw $validationResult["exception"];
      return;
    }
    $eventData = $this->buildEventDataObject($productId, $shelfPrice, $permalink, $attrs);
    return $this->sendEvent(self::EVENTS["product"], $eventData);
  }
  
  /**
   * Send product payment confirmation event to QL
   * @param $productId (string) - globally unique product id
   * @param $salePrice (double) - product sale price (must be a positive double)
   * @return array SDK request response object with body, errors and HTTP status
   */
  public function sendConfirmationEvent($productId, $salePrice)
  {
    $validationResult = $this->validateConfirmationEvent($productId, $salePrice);
    if ($validationResult["valid"] === false) {
      throw $validationResult["exception"];
      return;
    }
    $eventData = $this->buildEventDataObject($productId, $salePrice);
    return $this->sendEvent(self::EVENTS["conversion"], $eventData);
  }
  
  /**
   * Builds SDK event request URL
   * @param $event (string) - event name (product / confirmation)
   * @param $data (map) - event data
   * @return string event URL
   */
  public function buildEventURL($event, $data)
  {
    $payload = json_encode(array(
      "data" => $data,
      "name" => $event,
      "client_key" => $this->client_key,
    ));
    $ts = time() * 1000;
    $encodedPayload = urlencode($payload);
    $qs = join("&", array("event=$encodedPayload", "ts=$ts"));
    return sprintf("%s?%s", self::EVT_URL, $qs);
  }
  
  /**
   * Sends an SDK event to QL
   * @param $event (string) - event name (product / confirmation)
   * @param $data (map) - event data
   * @return array SDK request response object with body, errors and HTTP status
   */
  public function sendEvent($event, $data)
  {
    $url = $this->buildEventURL($event, $data);
    return $this->doRequest($url);
  }
  
  /**
   * Builds event data object with product id, price and optional permalink
   * @param $productId (string) - globally unique product id
   * @param $price (double) - event price (most be a positive double)
   * @param $permalink (string) - optional product permalink
   * @param $attrs (array) - associative array of additional product attributes
   * @return array event data as map
   */
  public function buildEventDataObject($productId, $price, $permalink = "", $attrs = array())
  {
    $data = array(
      "uid" => $productId,
      "meta" => array(
        "price" => $price
      ),
    );
    if ($permalink !== "") {
      $data["meta"]["permalink"] = $permalink;
    }
    $metaAttrs = array('ma_price', 'display_image', 'currency', 'inventory');
    foreach ($attrs as $key => $value) {
      if (in_array($key, $metaAttrs)) {
        $data['meta'][$key] = $value;
      } else {
        $data[$key] = $value;
      }
    }
    return $data;
  }
  
  /**
   * Validates product event data
   * @param $productId (string) - globally unique product id
   * @param $shelfPrice (double) - product shelf price (must be a positive double)
   * @param $permalink (string) - product page permalink
   * @return array containing valid flag and optional exception in case event is invalid
   */
  public function validateProductEvent($productId, $shelfPrice, $permalink) {
    $exception = null;
    $valid = true;
    if (gettype($productId) !== "string" || $productId === "") {
      $exception = new \InvalidArgumentException(sprintf("productId must be a valid non-empty string. got '%s' instead", $productId));
      $valid = false;
    }
    if (gettype($shelfPrice) !== "double" || $shelfPrice <= 0) {
      $exception = new \InvalidArgumentException("shelfPrice must be a positive double. got '$shelfPrice' instead");
      $valid = false;
    }
    if (gettype($permalink) !== "string" || $permalink === "") {
      $exception = new \InvalidArgumentException(sprintf("permalink must be a valid non-empty string. got '%s' instead", $permalink));
      $valid = false;
    }
    return array("valid" => $valid, "exception" => $exception);
  }
  
  /**
   * Validates payment confirmation event
   * @param $productId (string) - globally unique product id
   * @param $salePrice (double) - product sale price (must be a positive double)
   * @return array containing valid flag and optional exception in case event is invalid
   */
  public function validateConfirmationEvent($productId, $salePrice) {
    $exception = null;
    $valid = true;
    if (gettype($productId) !== "string" || $productId === "") {
      $exception = new \InvalidArgumentException(sprintf("productId must be a valid non-empty string. got '%s' instead", $productId));
      $valid = false;
    }
    if (gettype($salePrice) !== "double" || $salePrice <= 0) {
      $exception = new \InvalidArgumentException("salePrice must be a positive double. got '$salePrice' instead");
      $valid = false;
    }
    return array("valid" => $valid, "exception" => $exception);
  }
  
  /**
   * Perform HTTP request that sends the event to QL
   * @param $url (string) - SDK request URL with event data in query-string
   * @return array SDK request response object with body, errors and HTTP status
   */
  private function doRequest($url) {
    $curl_options = array(
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => $url,
      CURLOPT_FOLLOWLOCATION => true
    );
    $curl = curl_init();
    curl_setopt_array($curl, $curl_options);
    $result = curl_exec($curl);
    $errors = curl_error($curl);
    $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    return array("body" => $result, "errors" => $errors, "status_code" => $status_code);
  }
  
}